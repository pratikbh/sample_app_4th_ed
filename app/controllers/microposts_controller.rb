class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :find_micropost,   only: [:repost, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def repost
    if @micropost.repost!(current_user)
      flash[:success] = "Repost success!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(
        :content,
        :picture,
        :parent_id
      )
    end

    def find_micropost
      @micropost = Micropost.find_by(id: params[:id])
    end

    def correct_user
      redirect_to root_url if @micropost.nil?
    end
end
