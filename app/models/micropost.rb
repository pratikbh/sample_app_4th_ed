class Micropost < ApplicationRecord
  default_scope -> { order(created_at: :desc) }

  mount_uploader :picture, PictureUploader

  belongs_to :user
  belongs_to :parent_post,
    class_name: 'Micropost',
    foreign_key: :parent_id,
    inverse_of: :child_posts,
    optional: true

  has_many :child_posts,
    -> { where.not(content: nil) },
    class_name: 'Micropost',
    foreign_key: :parent_id,
    inverse_of: :parent_post
  has_many :followers,
    through: :user

  validates :user_id,
    presence: true
  validates :content,
    presence: true,
    length: { maximum: 140 },
    if: proc { |post| !post.parent_id? }
  validate  :picture_size

  def post_type
    if parent_id? && !content?
      'repost'
    elsif parent_id?
      'comment'
    else
      'post'
    end
  end

  def repost!(user)
    child_posts.create(user: user)
  end

  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
