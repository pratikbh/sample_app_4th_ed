class AddParentIdToMicroposts < ActiveRecord::Migration[5.1]
  def change
    add_reference :microposts, :parent, foreign_key: true
  end
end
