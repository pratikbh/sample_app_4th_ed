require 'rails_helper'

RSpec.describe Micropost, type: :model do
  fixtures :users
  fixtures :microposts

  let(:micropost_van) { microposts(:van) }
  let(:comment) { microposts(:comment) }
  let(:repost) { microposts(:repost) }
  let(:user) { users(:michael) }
  let(:lana) { users(:lana) }
  let(:micropost) { user.microposts.build(content: 'Lorem ipsum') }

  subject { micropost }

  describe 'Validations' do

    context 'When parent_id is present' do

      it { expect(comment).to_not validate_presence_of(:content) }
    end

    context 'When parent_id is blank' do

      it { expect(micropost_van).to validate_presence_of(:content) }
    end

  end

  describe 'Associations' do

    it do
      is_expected.to belong_to(:parent_post)
        .class_name('Micropost')
        .with_foreign_key(:parent_id)
        .optional
    end

    it do
      is_expected.to have_many(:child_posts)
        .class_name('Micropost')
        .with_foreign_key(:parent_id)
    end

    it do
      is_expected.to have_many(:followers)
    end

    it do
      child_post = micropost.child_posts.build(content: 'I am comment')
      expect(micropost.child_posts).to include(child_post)
      expect(child_post.parent_post).to eq(micropost)
    end

  end

  describe 'Instance Methods' do

    describe '#post_type' do

      it 'returns repost when content is blank and parent id is present' do
        expect(repost.post_type).to eq('repost')
      end

      it 'returns comment when content and parent id is present' do
        expect(comment.post_type).to eq('comment')
      end

      it 'returns micropost when parent_id is nil' do
        expect(micropost_van.post_type).to eq('post')
      end

    end


    describe '#repost!' do

      it 'creates repost for given user' do
        repost = micropost_van.repost!(lana)
        expect(repost.parent_id).to eq(micropost_van.id)
        expect(repost.user_id).to eq(lana.id)
      end

    end


  end

end
