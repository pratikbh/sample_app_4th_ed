require 'rails_helper'

RSpec.describe User, type: :model do

  fixtures :users
  fixtures :microposts
  fixtures :relationships

  let(:lana) { users(:lana) }

  describe 'Instance Methods' do

    describe '#feed' do

      it 'returns feed from followings' do
        expect(lana.feed).to include(microposts(:cat_video))
      end

      it 'returns feed from self' do
        expect(lana.feed).to include(microposts(:van))
      end

      it 'does not return comments' do
        expect(lana.feed).to_not include(microposts(:comment))
      end


      it 'returns reposts' do
        expect(lana.feed).to_not include(microposts(:repost))
      end

      it 'does not return feed from non followings' do
        expect(lana.feed).to_not include(microposts(:zone))
      end

    end

  end

end
